# Confluence Attachment Report

This tool is used for creating a report of the size of attachments added to incidents over the last year


## Getting Started

You will need an account in confluence with access to read all the incidents in the projects you are interested in, and you will also need to obtain a token for the account you can [Request a token here](https://confluence.atlassian.com/cloud/api-tokens-938839638.html)

### Prerequisites

You will need at least v3.1 of the ASP.NET Core SDK

* [.NET Core SDK 3.1 LTS](https://dotnet.microsoft.com/download/visual-studio-sdks) - Select the 64bit SDK for 3.1 and Visual studio 2019

![.NET Core SDK 3.1 LTS visual studio 2019](readmeImages/netSDK.png)

```
Some development IDE, e.g. Visual Studio 2019 or later
```


### Installing

A step by step series of examples that tell you how to get a development env running

* Download this repository locally
* Open in IDE
* Copy the file in Confluence.AttachmentReports.ConsoleApp\appsettings.json and call the copy appsettings.Development.json

```Note: This is the format of the JSON (appsettings.Development.json) used to store credentials for the environment you wish to connect to. ```

```json
{
  // Copy this file to appsettings.Development.json and edit the values below

  "confluence": {
    "url": "NOT SET", // setting env var Confluence__confluence__url will override this
    "username": "NOT SET", // setting env var Confluence__confluence__username will override this
    "token": "NOT SET" // setting env var Confluence__confluence__token will override this
  }
}
```

* Update the values in the JSON file and save



## Running the project locally

* Run the project in either release or debug
* The chart will be created in C:\temp\chart.svg make sure the path C:\temp\ exists or search and replace for C:\temp\ to update to new location
* All dependencies are stored in Nuget
* Output like below should be displayed

![output](readmeImages/consoleoutput.png)


```In the C:\temp you will see a chart.svg as below ```

![output](readmeImages/chart.svg)

## Debugging

If you have any issues, enable tracing to switch on debug output by uncommenting this line in ConfluenceTools\ConfluenceClientHelper.cs

```csharp
//settings.EnableRequestTrace = true;

```

### Coding style

Coding guidelines, are default Resharper recommendations.

## Deployment

* Commit to GIT, create a new branch for all features and then raise a PR to get the branch merged to master

## Contributing

Please follow normal Git branching strategy, with a new branch for each feature

## Versioning

no tagging strategy defined

## Authors

* **Mark Richardson** - *Initial work* - [Tridion Ted](https://twitter.com/tridionted)


## License

This project is closed source.

## Acknowledgments

* Stack exchange