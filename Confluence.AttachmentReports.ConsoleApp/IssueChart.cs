﻿using ConfluenceTools;
using OxyPlot.Axes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Confluence.AttachmentReports.ConsoleApp
{
    internal class IssueChart
    {
        internal static void CreateChart(IEnumerable<IssueAttachmentSummary> issueAttachments, int monthsToGoBack, string xAxisFormat)
        {
            var graphData = new GraphData();

            


            foreach (var project in issueAttachments.Select(i=> i.Project).Distinct())
            {
                var graphChannelRecord = new GraphChannelRecord();
                graphChannelRecord.Name = project;

                graphData.ChannelRecords.Add(graphChannelRecord);
                for (int i = monthsToGoBack-1; i > -1; i--)
                {
                    var metricDate = DateTime.UtcNow.AddMonths(-i);
                    var firstDayOfMonth = new DateTime(metricDate.Year, metricDate.Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1);
                    var storageAddedThisMonth = issueAttachments.Where(i => i.Project.Equals(project, StringComparison.InvariantCultureIgnoreCase) && i.CreatedDate > firstDayOfMonth && i.CreatedDate < lastDayOfMonth).Sum(i=> i.FileSize);
                    var storageAddedThisMonthValue = ByteSize.FromBytes(storageAddedThisMonth).MegaBytes;
                    graphChannelRecord.values.Add(firstDayOfMonth, storageAddedThisMonthValue);
                    Console.WriteLine($"{project} {metricDate} {storageAddedThisMonthValue}MB");
                }



                DateTimeAxis.ToDouble(DateTime.Now);
                ChartTool.CreateChart(graphData, xAxisFormat);
            }
        }
    }
}