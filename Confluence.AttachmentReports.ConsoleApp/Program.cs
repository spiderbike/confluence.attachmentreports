﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using ConfluenceTools;
using Figgle;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using OxyPlot.Axes;

namespace Confluence.AttachmentReports.ConsoleApp
{
    class MainService : IHostedService
    {
        private readonly IHostApplicationLifetime _appLifetime;
        private readonly IssueHelper _issueHelper;
        private readonly ConfluenceClientHelper _confluenceClient;

        public MainService(IHostApplicationLifetime appLifetime, IssueHelper issueHelper, ConfluenceClientHelper confluenceClient)
        {
            _appLifetime = appLifetime;
            _issueHelper = issueHelper;
            _confluenceClient = confluenceClient;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {

            await Console.Out.WriteLineAsync(FiggleFonts.Slant.Render("Confluence Tools")).ConfigureAwait(false);
            await Console.Out.WriteLineAsync($"Confluence Tools {Assembly.GetExecutingAssembly().GetName().Version}").ConfigureAwait(false);

            var issueAttachments = await _issueHelper.GetAttachmentsForIssues();
            int item = 0;
            foreach (var issueAttachment in issueAttachments)
            {
                item++;
                await Console.Out.WriteLineAsync($"{item}   **** {issueAttachment.Project}  {issueAttachment.IssueKey} {issueAttachment.CreatedDate}  {issueAttachment.FileName} -- {issueAttachment.MimeType}  {issueAttachment.FileSize}").ConfigureAwait(false);

            }


            IssueChart.CreateChart(issueAttachments, 12, "MMM-yyyy");


            if (Debugger.IsAttached)
            {
                await Console.Out.WriteLineAsync("press any key to exit.").ConfigureAwait(false);
                Console.ReadKey();
            }
            _appLifetime.StopApplication();
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }


    class Program
    {
        private const string AppSettings = "appsettings.json";
        private const string HostSettings = "hostsettings.json";

        static Task Main(string[] args)
        {
            var consoleTracer = new ConsoleTraceListener(true);

            // Set the name of the trace listener, which helps identify this
            // particular instance within the trace listener collection.
            consoleTracer.Name = "mainConsoleTracer";

            // Write the initial trace message to the console trace listener.
            consoleTracer.WriteLine(DateTime.Now.ToString() + " [" + consoleTracer.Name + "] - Starting output to trace listener.");

            // Add the new console trace listener to
            // the collection of trace listeners.
            Trace.Listeners.Add(consoleTracer);

            var builder = new HostBuilder()
                .ConfigureHostConfiguration(configHost =>
                {
                    configHost.SetBasePath(Directory.GetCurrentDirectory());
                    configHost.AddJsonFile(HostSettings, optional: true);
                    configHost.AddCommandLine(args);
                })
                .ConfigureAppConfiguration((hostContext, configApp) =>
                {
                    configApp.SetBasePath(Directory.GetCurrentDirectory());
                    configApp.AddJsonFile(AppSettings, optional: true);
                    configApp.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", optional: true);
                    configApp.AddEnvironmentVariables(prefix: "Confluence_");
                    configApp.AddCommandLine(args);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<ConsoleLifetimeOptions>(options =>
                    {
                        options.SuppressStatusMessages = true;
                    });



                    services.AddSingleton<IssueHelper>();
                    services.AddSingleton<ConfluenceClientHelper>();


                    services.AddSingleton<IHostedService, MainService>();
                    services.Configure<ConfluenceSettings>(hostContext.Configuration.GetSection("confluence"));
                });

            return builder.RunConsoleAsync();
        }
    }

}
