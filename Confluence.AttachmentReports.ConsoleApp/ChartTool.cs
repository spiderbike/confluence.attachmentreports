﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Confluence.AttachmentReports.ConsoleApp
{
    class ChartTool
    {
        public static void CreateChart(GraphData graphData, string xAxisFormat)
        {
            var myModel = new PlotModel { Title = "Attachment space utilization"};

            foreach (var channelRecord in graphData.ChannelRecords)
            {
                var fs = new FunctionSeries { Title = channelRecord.Name, StrokeThickness = 3};
                foreach (var valueRecord in channelRecord.values)
                {
                    fs.Points.Add(new DataPoint(DateTimeAxis.ToDouble(valueRecord.Key), valueRecord.Value));
                }
                myModel.Series.Add(fs);
            }



            myModel.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = "storage MB", FontSize = 20, AxisTitleDistance = 15 });
            myModel.Axes.Add(new DateTimeAxis { Position = AxisPosition.Bottom , StringFormat = xAxisFormat });


            using var stream = File.Create("C:\\temp\\chart.svg");
            var exporter = new SvgExporter { Width = 600, Height = 400 };
            exporter.Export(myModel, stream);
        }

    }

    public class GraphData
    {
        public List<GraphChannelRecord> ChannelRecords { get; set; } = new List<GraphChannelRecord>();
    }

    public class GraphChannelRecord
    {
        public string Name { get; set; }
        public Dictionary<DateTime, double> values { get; set; } = new Dictionary<DateTime, double>();
    }
}
