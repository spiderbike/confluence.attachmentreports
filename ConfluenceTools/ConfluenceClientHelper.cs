﻿using System.Threading.Tasks;
using Atlassian.Jira;
using Microsoft.Extensions.Options;


namespace ConfluenceTools
{
    public class ConfluenceClientHelper
    {
        private readonly ConfluenceSettings _confluenceSettings;
        private Jira _jira;

        public ConfluenceClientHelper(IOptions<ConfluenceSettings> confluenceSettings)
        {
            _confluenceSettings = confluenceSettings.Value;
        }

        public async Task<Jira> GetClient()
        {
            var settings = new JiraRestClientSettings();
            //settings.EnableRequestTrace = true;
            if (_jira == null)
            {
                _jira = Jira.CreateRestClient(_confluenceSettings.Url, _confluenceSettings.Username, _confluenceSettings.Token, settings);
            }

            return _jira;
        }
    }
}