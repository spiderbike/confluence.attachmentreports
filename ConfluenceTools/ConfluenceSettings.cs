﻿namespace ConfluenceTools
{
    public class ConfluenceSettings
    {
        public string Url { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
    }
}